# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import os
import pandas as pd
from pypdf import PdfWriter, PdfReader
import slugify
import json

input_folder = "outputs/steps/3_pdf/"
output_folder = "outputs/steps/4_online_pdfs/"

# Get the type of territory from config.json
with open("config.json", "r") as f:
    config = json.load(f)
    
TYPE = config["territory_type"]  # Change this to "departement" or "epci" as needed in config.json

# List of valid types
valid_types = ["epci", "departement", "scot"]

# Check if the TYPE is valid
if TYPE not in valid_types:
    raise ValueError("Invalid type of territory")

# Dynamically construct the file name for territories list
territories_file = f"inputs/csv/{TYPE}.csv"

# Load territories list
territories_list = pd.read_csv(territories_file, sep=";", dtype={'code': str})

GS_EXEC_NAME = "gs"

for index, epci in territories_list.iterrows():
    epci_name = epci.nom
    siren = str(epci.code)

    filename = f"{siren}-{slugify.slugify(epci_name)}.pdf"
    if not os.path.isfile(f"{input_folder}{filename}"):
        print("WARNING, missing file " + epci_name)
        continue
    with open(f"{input_folder}{filename}", "rb") as input_file:
        # opening input file
        input_pdf = PdfReader(input_file)
        # opening output file
        output_pdf = PdfWriter()

        for page in input_pdf.pages:
            # cropping
            page.cropbox.lower_left = (
                23,
                23,
            )
            page.cropbox.upper_right = (
                page.mediabox.right - 23,
                page.mediabox.top - 23,
            )

            output_pdf.add_page(page)

        output_filename = filename
        # saving
        with open(f"{output_folder}{output_filename}", "wb") as out_f:
            output_pdf.write(out_f)
            print("File saved to", output_filename)
