
-- DELETE UNIQUE CONSTRAINT KEY (nom, region) IF EXISTS
ALTER TABLE meta.indicateur
DROP CONSTRAINT IF EXISTS indicateur_nom_region_key;

-- DELETE FOREIGN CONSTRAINT
ALTER TABLE meta.chart
DROP CONSTRAINT chart_indicateur_fkey;

-- ADD ON CASCADE DELETE ON FORIEGN KEY
ALTER TABLE meta.chart
ADD CONSTRAINT chart_indicateur_fkey
FOREIGN KEY (indicateur)
REFERENCES meta.indicateur(id)
ON DELETE CASCADE;

-- DELETE all rows with key NOT NULL to avaid duplication
DELETE FROM meta.indicateur where key is not null;


-- INSERT INTO meta.indicateur

-- MAKE SURE YEARS are AVAILABLE IN DATA TABLE 
-- MAKE SURE DATA TABLE EXISTS IN REGIONAL SCHEMA OTHERWISE ADD THEM TO SCRIPT_CREATE_MISSING_TABLE.sql 

INSERT INTO meta.indicateur (key, nom, data, type, color_start, color_end, unit, active, years, ui_theme, decimals, concatenation, region)
VALUES
('besoins-indus-chaleur', 'Besoins industriels en chaleur' , 'res_dhc_besoin_chaleur_industriel', 'circle', '#ffffff', '#d0021b', 'GWh', 't', '{2018}', 'Chaleur renouvelable', 2,NULL, 'auvergne-rhone-alpes'),
('domicile-travail', 'Distance domicile - travail' , 'mobilite_insee', 'circle', '#ffffff', '#007f7b', 'personnes', 't', '{2017}', 'Mobilité', 0, NULL, 'auvergne-rhone-alpes'),
('emplois-salarie-secteur', 'Emplois salariés par secteur' , 'analyse_clap', 'circle', '#007f7b', '#007f7b', 'emplois', 't', '{2019}', 'Économie et société', 0, NULL,  'auvergne-rhone-alpes'),
('conso-ener', 'Consommation d''énergie', 'conso_energetique', 'circle', '#007f7b', '#007f7b', 'GWh', 't', '{2022,2021,2020,2015}', 'Consommation d''énergie', 0, NULL, 'auvergne-rhone-alpes'),
('potentiel-metha', 'Potentiel de méthanisation (hors biodéchet de l''industrie agroalimentaire)' , 'potentiel_methanisation*0.001', 'circle', '#007f7b', '#007f7b', 'GWh', 't', '{2022,2021,2020,2019,2018,2017,2016,2015}', 'Potentiels ENR', 0, NULL, 'auvergne-rhone-alpes'),
('potentiel-bois', 'Potentiel bois : surfaces exploitables' , 'potentiel_bois', 'circle', '#007f7b', '#007f7b', 'ha', 't', '{2022}', 'Potentiels ENR', 2, NULL, 'auvergne-rhone-alpes'),
('emission-ges', 'Émissions GES' , 'emission_ges', 'circle', '#007f7b', '#007f7b', 'kt eq. CO₂', 't', '{2022,2021,2020,2015}', 'Émissions de GES', 2, NULL, 'auvergne-rhone-alpes'),
('prod-enr', 'Production ENR' , 'prod_enr', 'circle', '#007f7b', '#007f7b', 'GWh', 't', '{2022,2021,2020,2015}', 'Production d''énergie', 2, NULL, 'auvergne-rhone-alpes'),
('prod-pv', 'Production photovoltaïque' , 'prod_enr_pv', 'circle', '#007f7F', '#007f7F', 'GWh', 't', '{2022,2021,2020,2015}', 'Production d''énergie', 2, NULL, 'auvergne-rhone-alpes'),
('prod-metha', 'Production méthanisation' , 'prod_enr_methanisation', 'circle', '#007f7F', '#007f7F', 'GWh', 't', '{2022,2021,2020,2015}', 'Production d''énergie', 2,NULL, 'auvergne-rhone-alpes'),
('prod-bois-biomasse', 'Production bois/biomasse' , 'prod_enr_bois', 'circle', '#007f7F', '#007f7F', 'GWh', 't', '{2022,2021,2020,2015}', 'Production d''énergie', 2, NULL, 'auvergne-rhone-alpes'),
('polluants-atmo', 'Polluants atmosphériques' , 'air_polluant', 'null', '#007f7b', '#007f7b', 't-eq. CO2', 't', '{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005}', 'Emissions de polluants atmosphériques', 2, '{2,3,4,5,6,7}', 'auvergne-rhone-alpes'),
('logement-ener', 'DPE Logements : étiquette énergie' , 'logement_energie_dpe', 'circle', '#ffffff', '#007f7b', 'logements', 't', '{2019}', 'Bâtiments', 0, NULL, 'auvergne-rhone-alpes'),
('densite-population', 'Densité de population' , 'population/maille.superficie_cadastrale', 'choropleth', '#ffffff', '#007f7b', 'hab./km²', 't', '{2021}', 'Économie et société', 2, NULL, 'auvergne-rhone-alpes'),
('population', 'Population' , 'population', 'circle', '#007f7b', '#007f7b', 'hab.', 't', '{2021}', 'Économie et société', 0, NULL, 'auvergne-rhone-alpes'),
('facture-ener', 'Facture énergétique' , 'facture_energetique*0.001', 'circle', '#007f7b', '#007f7b', 'k€', 't', '{2021,2020,2019,2018,2017,2016,2015}', 'Facture énergétique', 0, NULL, 'auvergne-rhone-alpes'),
('potentiel-solaire-pv', 'Potentiel solaire photovoltaïque' , 'potentiel_photovoltaique*0.001', 'circle', '#007f7b', '#007f7b', 'GWh', 't', '{2022}', 'Potentiels ENR', 0, NULL, 'auvergne-rhone-alpes'),
('part-enr-conso-ener', 'Production d''EnR' , 'prod_enr', 'choropleth', '#ffffff', '#006961', '%', 't', '{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011}', 'Production d''énergie', 2, NULL, 'auvergne-rhone-alpes'),
('air_polluant_covnm','Émissions de COVNM' ,'air_polluant_covnm','circle','#007f7b','#007f7b','t','t','{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000}','Emissions de polluants atmosphériques',0, NULL,'auvergne-rhone-alpes'),
('air_polluant_nh3','Émissions de NH3' ,'air_polluant_nh3','circle','#007f7b','#007f7b','t','t','{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000}','Emissions de polluants atmosphériques',0, NULL,'auvergne-rhone-alpes'),
('air_polluant_nox','Émissions de NOx' ,'air_polluant_nox','circle','#007f7b','#007f7b','t','t','{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000}','Emissions de polluants atmosphériques',0, NULL,'auvergne-rhone-alpes'),
('air_polluant_sox','Émissions de SO2' ,'air_polluant_sox','circle','#007f7b','#007f7b','t','t','{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000}','Emissions de polluants atmosphériques',0, NULL,'auvergne-rhone-alpes'),
('air_polluant_pm25','Émissions de PM2.5' ,'air_polluant_pm25','circle','#007f7b','#007f7b','t','t','{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000}','Emissions de polluants atmosphériques',0, NULL,'auvergne-rhone-alpes'),
('air_polluant_pm10','Émissions de PM10' ,'air_polluant_pm10','circle','#007f7b','#007f7b','t','t','{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2010,2005,2000}','Emissions de polluants atmosphériques',0, NULL,'auvergne-rhone-alpes');






-- mapping indicateur tableau_thematique

-- ('besoins-indus-chaleur', 196),
-- ('domicile-travail', 75),
-- ('emplois-salarie-secteur', 36),
-- ('conso-ener', 1),
-- ('potentiel-metha', 24),
-- ('potentiel-bois', 26),
-- ('emission-ges', 29),
-- ('prod-enr', 46),
-- ('prod-pv', 15122),
-- ('prod-metha', 15123),
-- ('prod-bois-biomasse', 15124),
-- ('polluants-atmo', 102),
-- ('logement-ener', 143),
-- ('densite-population', 204),
-- ('population', 203),
-- ('facture-ener', 19),
-- ('potentiel-solaire-pv', 28),
-- ('part-enr-conso-ener', 23),
-- ('air_polluant_covnm', 2),
-- ('air_polluant_nh3', 3),
-- ('air_polluant_nox', 4),
-- ('air_polluant_sox', 5),
-- ('air_polluant_pm25', 6),
-- ('air_polluant_pm10', 7);


