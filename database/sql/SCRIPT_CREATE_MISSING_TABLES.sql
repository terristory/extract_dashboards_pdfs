DO $_$
BEGIN
    -- Check and create prod_enr_pv table
    IF NOT EXISTS (
        SELECT 1
        FROM pg_catalog.pg_tables
        WHERE schemaname = 'auvergne_rhone_alpes'
          AND tablename = 'prod_enr_pv'
    ) THEN
        CREATE TABLE auvergne_rhone_alpes.prod_enr_pv 
        AS SELECT
            annee, type_prod_enr AS type_prod_enr_pv, valeur, commune
        FROM
            auvergne_rhone_alpes.prod_enr
        WHERE
            type_prod_enr = ANY(
                SELECT
                    modalite_id
                FROM
                    meta.categorie
                WHERE
                    nom = 'type_prod_enr'
                    AND region LIKE 'auv%'
                    AND modalite IN ('Production photovoltaïque', 'Production du solaire thermique')
            );
    END IF;

    -- Check and create prod_enr_methanisation table
    IF NOT EXISTS (
        SELECT 1
        FROM pg_catalog.pg_tables
        WHERE schemaname = 'auvergne_rhone_alpes'
          AND tablename = 'prod_enr_methanisation'
    ) THEN
        CREATE TABLE auvergne_rhone_alpes.prod_enr_methanisation 
        AS SELECT
            annee, type_prod_enr, valeur, commune
        FROM
            auvergne_rhone_alpes.prod_enr
        WHERE
            type_prod_enr = ANY(
                SELECT
                    modalite_id
                FROM
                    meta.categorie
                WHERE
                    nom = 'type_prod_enr'
                    AND region LIKE 'auv%'
                    AND modalite IN ('Valorisation électrique du biogaz', 'Valorisation thermique du biogaz', 'Valorisation par injection de biométhane')
            );
    END IF;
END $_$;
