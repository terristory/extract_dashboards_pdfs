import os
import psycopg2
import json
import argparse
import logging
import requests
import pandas as pd
import datetime

# Configure logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")
logger = logging.getLogger(__name__)

# Function to clear the content of a CSV file
def clear_csv(file_path):
    """
    Clears the content of a CSV file.
    """
    with open(file_path, 'w') as f:
        f.truncate(0)
        logger.info(f"CLEARED {file_path}")


# Function to read queries from a .sql file
def read_queries_from_file(sql_file):
    """
    Reads queries from a .sql file.
    """
    queries = {}
    with open(sql_file, 'r') as file:
        content = file.read()

    # Split the file into sections by comments starting with '-- '
    query_sections = content.split('-- ')
    for section in query_sections[1:]:  # Skip the first split (before the first '-- ')
        lines = section.split('\n')
        query_name = lines[0].strip()  # The first line is the query name
        query_body = '\n'.join(lines[1:]).strip()  # The rest is the query body
        queries[query_name] = query_body

    return queries

# Function to execute COPY commands based on queries from a .sql file
def execute_copy_from_sql_file(sql_file, db_config, csv_mapping):
    """
    Executes COPY commands based on queries read from a .sql file.
    sql_file: Path to the .sql file containing queries.
    csv_mapping: Dictionary mapping query names to CSV file paths.
    """
    try:
        # Load queries from the .sql file
        queries = read_queries_from_file(sql_file)

        # Connect to PostgreSQL database
        connection = psycopg2.connect(**db_config)
        cursor = connection.cursor()

        for query_name, query in queries.items():
            if query_name in csv_mapping:
                csv_path = csv_mapping[query_name]

                # Ensure the parent directory exists
                os.makedirs(os.path.dirname(csv_path), exist_ok=True)

                # Clear the existing CSV file content
                logger.info(f"CLEARING Files")
                clear_csv(csv_path)

                # Execute the COPY command
                copy_command = f"COPY ({query}) TO '{csv_path}' CSV HEADER DELIMITER ';';"
                cursor.execute(copy_command)
                logger.info(f"Exported {query_name} to {csv_path}")

        connection.commit()
        logger.info("Data export completed successfully.")

    except Exception as e:
        logger.error(f"An error occurred: {e}", exc_info=True)

    finally:
        if 'connection' in locals():
            cursor.close()
            connection.close()
            logger.info("Database connection closed.")


def get_prod_enr_jauge():
    """
        Retrieve the prod enr rate from TerriSTORY® api.    
    """
    # Date parsing function
    dateparse = lambda x: datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f')

    # Get the type of territory from config.json
    with open("config.json", "r") as f:
        config = json.load(f)

    TYPE = config["territory_type"]  # Change this to "departement" or "epci" as needed in config.json

    # List of valid types
    valid_types = ["epci", "departement", "scot"]

    # Check if the TYPE is valid
    if TYPE not in valid_types:
        raise ValueError("Invalid type of territory")

    # Dynamically construct the file name for territories list
    territories_file = f"{config['directory_path']}/inputs/csv/{TYPE}.csv"

    # Clear the CSV file before processing
    clear_csv(f"{config['directory_path']}/inputs/csv/prod_enr_rate.csv")

    # Load territories list
    territories = pd.read_csv(territories_file, sep=";")

    vals = []
    for index, territory in territories.iterrows():
        territory_name = territory.nom
        siren = territory.code
        url = f"http://localhost:8080/api/auvergne-rhone-alpes/analysis/23/graphique/jauge-circulaire?zone={TYPE}&maille=commune&zone_id={siren}"
        body = "{}"

        rep = requests.post(url, body)
        vals.append({"territory": siren, "val": float(rep.content)})

    df = pd.DataFrame.from_dict(vals)
    df["date"] = datetime.datetime.now()
    df["date"] = pd.to_datetime(df["date"])
    df["territory"] = df["territory"].astype(str)
    df.to_csv(f"{config['directory_path']}/inputs/csv/prod_enr_rate.csv", sep=";", index=False)


def main(config_file):
    """
    Main function to load configuration, set up CSV mapping, and execute the export process.
    """
    # Load configuration
    logger.info(f"**********Fill csv files from database**********")
    logger.info(f"Loading configuration from file: {config_file}")
    with open(config_file, "r") as f:
        config = json.load(f)
    logger.info("Configuration loaded successfully.")

    # Validate directory path
    directory_path = config.get("directory_path")
    if not directory_path:
        logger.error("Directory path not specified in the configuration file.")
        return

    # Path to the .sql file containing the queries
    sql_file_path = "sql/SCRIPT_EXPORT_INPUT_CSV.sql"
    if not os.path.isfile(sql_file_path):
        logger.error(f"SQL file not found: {sql_file_path}")
        return

    # Mapping of query names in the .sql file to CSV file paths
    csv_mapping = {
        "EPCI": f"{directory_path}/inputs/csv/epci.csv",
        "DEPARTEMENT": f"{directory_path}/inputs/csv/departement.csv",
        "SCOT": f"{directory_path}/inputs/csv/scot.csv",
        "METHANISATION_EPCI": f"{directory_path}/inputs/csv/methanisation_epci.csv",
        "PV_EPCI": f"{directory_path}/inputs/csv/pv_epci.csv",
        "BOIS_EPCI": f"{directory_path}/inputs/csv/bois_epci.csv",
        "METHANISATION_DEPARTEMENT": f"{directory_path}/inputs/csv/methanisation_departement.csv",
        "PV_DEPARTEMENT": f"{directory_path}/inputs/csv/pv_departement.csv",
        "METHANISATION_SCOT": f"{directory_path}/inputs/csv/methanisation_scot.csv",
        "PV_SCOT": f"{directory_path}/inputs/csv/pv_scot.csv",
    }

    # Execute the function
    execute_copy_from_sql_file(sql_file_path, config["db_params"], csv_mapping)

    # Get pod enr data
    get_prod_enr_jauge()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="TerriSTORY® territorial file export data script")
    parser.add_argument(
        "-c", "--config", type=str, help="Config file", default="config.json"
    )
    args = parser.parse_args()
    main(args.config)