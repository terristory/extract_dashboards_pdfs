import psycopg2
import subprocess
import json
from string import Template
import argparse
import logging
import csv


# Configure logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")
logger = logging.getLogger(__name__)

def read_sql_file(file_path, **kwargs):
    """
    Reads the SQL file and performs string substitution with kwargs.
    """
    logger.info(f"Reading SQL file: {file_path}")
    with open(file_path, "r") as file:
        content = file.read()
    logger.info("SQL file read successfully.")
    return Template(content).safe_substitute(kwargs)


def execute_sql_script(cursor, sql_script, expects_result=False):
    """
    Executes the SQL script using the provided cursor, optionally fetching a result.
    """
    logger.info("Executing SQL script...")
    cursor.execute(sql_script)
    logger.info("SQL script executed successfully.")
    if expects_result:
        result = cursor.fetchone()
        if result:
            logger.info(f"Query returned result: {result[0]}")
            return result[0]
        else:
            logger.info("Query executed, but no results returned.")
    return None


def execute_sql_script_from_file(conn, sql_file_path, expects_result=False, **params):
    """
    Reads and executes SQL from a file with parameter substitution.
    """
    logger.info(f"Executing SQL script from file: {sql_file_path}")
    sql_script = read_sql_file(sql_file_path, **params)
    with conn.cursor() as cursor:
        result = execute_sql_script(cursor, sql_script, expects_result=expects_result)
    conn.commit()
    logger.info(f"SQL script from {sql_file_path} executed and committed successfully.")
    return result


def create_user_if_not_exists(conn, env_path, user_params):
    """
    Check if a user exists in the database; if not, create the user.
    """
    email = user_params["mail"]
    query = "SELECT mail FROM utilisateur WHERE mail = %s;"
    logger.info(f"Checking if user with email {email} exists in the database...")
    with conn.cursor() as cursor:
        cursor.execute(query, (email,))
        result = cursor.fetchone()
        if result:
            logger.info(f"User {email} already exists in the database.")
            return result[0]
        
        logger.info(f"User {email} does not exist. Creating user...")
        subprocess.run(
            f". {env_path}/bin/activate && "
            f"terriapi-user --mail {user_params['mail']} --prenom {user_params['prenom']} "
            f"--nom {user_params['nom']} --fonction \"{user_params['fonction']}\" "
            f"--organisation \"{user_params['organisation']}\" --region {user_params['territoire']} "
            f"--territoire {user_params['territoire']} --password {user_params['password']} "
            f"--profil {user_params['profil']} --actif",
            shell=True,
            check=True,
        )
        logger.info("User creation command executed successfully. Verifying user creation...")
        cursor.execute(query, (email,))
        result = cursor.fetchone()
        if result:
            logger.info(f"User {email} created successfully and exists in the database.")
            return result[0]
        raise Exception("Failed to create user or retrieve user ID.")


def update_thematique_and_insert(conn, dashboard_id, json_file_path):
    """
    Updates 'graphiques' in 'meta.tableau_thematique' and inserts updated data, reading rows from a JSON file.
    """
    print("Fetching 'key' to 'id_analysis' mapping from meta.indicateur...")
    with conn.cursor() as cursor:
        cursor.execute("SELECT key, id FROM meta.indicateur WHERE key IS NOT NULL")
        key_to_id_analysis = {row[0]: row[1] for row in cursor.fetchall()}
    print("Fetched key-to-id_analysis mapping successfully.")

    # Load rows from JSON file
    print(f"Loading data from JSON file: {json_file_path}")
    with open(json_file_path, "r") as file:
        rows = json.load(file)

    # Delete existing rows in meta.tableau_thematique for the given tableau_id
    print(f"Deleting existing rows from meta.tableau_thematique where tableau = {dashboard_id}...")
    with conn.cursor() as cursor:
        cursor.execute("""
            DELETE FROM meta.tableau_thematique
            WHERE tableau = %s;
        """, (dashboard_id,))
        print("Existing rows deleted successfully.")

    print("Updating graphiques with id_analysis...")
    for row in rows:
        for graphique in row["graphiques"]:
            key = graphique.get("key")
            if key in key_to_id_analysis:
                graphique["id_analysis"] = key_to_id_analysis[key]

    print("Inserting updated data into meta.tableau_thematique...")
    with conn.cursor() as cursor:
        for row in rows:
            # Replace tableau's placeholder with the integer dashboard_id
            row["tableau"] = int(dashboard_id)
            cursor.execute("""
                INSERT INTO meta.tableau_thematique (titre, tableau, graphiques, description, ordre)
                VALUES (%s, %s, %s, %s, %s)
            """, (
                row["titre"],
                row["tableau"],
                json.dumps(row["graphiques"]),  # Convert graphiques to JSON string
                row["description"],
                row["ordre"]
            ))
    conn.commit()
    print("Data inserted successfully into meta.tableau_thematique.")


def alter_table_add_column_if_needed(conn):
    """
    Alter the 'meta.indicateur' table to add the 'key' column if it doesn't already exist.
    """
    logger.info("Checking if the 'key' column exists in 'meta.indicateur'...")

    alter_table_query = """
    DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='indicateur' AND column_name='key') THEN
            ALTER TABLE meta.indicateur ADD COLUMN key VARCHAR(255);
        END IF;
    END;
    $$;
    """
    with conn.cursor() as cursor:
        cursor.execute(alter_table_query)
        conn.commit()
    logger.info("Table altered successfully.")


def get_tableau_id_if_exists(conn, tableau_name):
    """
    Checks if a dashboard with the given name exists and returns its ID if found.
    If the dashboard doesn't exist, returns None.
    """
    query = "SELECT id FROM meta.tableau_bord WHERE titre = %s;"
    with conn.cursor() as cursor:
        cursor.execute(query, (tableau_name,))
        result = cursor.fetchone()
        if result:
            logger.info(f"Tableau with name '{tableau_name}' exists. ID: {result[0]}")
            return result[0]
        else:
            logger.info(f"Tableau with name '{tableau_name}' does not exist.")
            return None


def create_or_get_tableau_id(conn, dashboard_params, user_params):
    """
    Fetch the dashboard ID if it exists, otherwise inserts a new one.
    """
    tableau_id = get_tableau_id_if_exists(conn, dashboard_params["name"])
    if tableau_id:
        print(f"Tableau already exists with ID: {tableau_id}")
        return tableau_id  # If the dashboard exists, return the existing ID

    try:
        # If the dashboard doesn't exist, insert it
        with conn.cursor() as cursor:
            cursor.execute("""
                INSERT INTO meta.tableau_bord (titre, description, mail, region)
                VALUES (%s, %s, %s, %s)
                RETURNING id;
            """, (dashboard_params["name"], dashboard_params["description"], user_params["mail"], user_params["territoire"]))
            
            result = cursor.fetchone()
            if result:
                tableau_id = result[0]
                print(f"Tableau inserted with ID: {tableau_id}")
                return tableau_id
            else:
                print("Tableau insertion failed.")
                return None
    except psycopg2.errors.UniqueViolation as e:
        print(f"UniqueViolation error: {e}. Tableau already exists.")
        # Return the existing dashboard ID
        return tableau_id 


def update_or_insert_categorie(conn, csv_file_path):
    """
    Checks for each row in meta.categorie if the 'nom' column exists.
    If it exists, deletes the old row and inserts the new row from the csv file. If it does not exist, inserts the row.
    """
    # Open the CSV file and read rows
    with open(csv_file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';') 
        rows = [row for row in reader]
    # Process each row from the CSV
    for row in rows:
        nom = row['nom']
        modalite_id = row['modalite_id']
        region = row['region']
        with conn.cursor() as cursor:
            # Check if the 'nom' already exists in meta.categorie
            cursor.execute("SELECT nom FROM meta.categorie WHERE nom = %s AND modalite_id = %s AND region = %s", (nom, modalite_id, region))
            existing = cursor.fetchone()

            if existing:
                # If the row exists, delete it and insert the new one to avoid duplication and conflict 
                print(f"Deleting existing row with nom = {nom} for modalite_id = {modalite_id} and region = {region}...")
                cursor.execute("DELETE FROM meta.categorie WHERE nom = %s AND modalite_id = %s AND region = %s", (nom, modalite_id, region))
                print(f"Deleted row with nom = {nom}, modalite_id = {modalite_id} AND region = {region}.")

            # Insert the new row (or after deletion)
            cursor.execute("""
                INSERT INTO meta.categorie (nom, modalite, modalite_id, couleur, ordre, region)
                VALUES (%s, %s, %s, %s, %s, %s)
                """, (row['nom'], row['modalite'], row['modalite_id'], row['couleur'], row['ordre'], row['region']))
            print(f"Inserted new row with nom = {nom}.")

            # Commit the changes
            conn.commit()

    print("Process completed successfully.")


def insert_indicateur_details_from_csv(conn, csv_file_path):
    """
    Inserts data into the meta.indicateur_representation_details table from the csv file.
    Links 'key' in the CSV to 'id' in meta.indicateur.
    Replaces keys in the 'name' column with corresponding 'id' from meta.indicateur.
    Ensures the 'key' column exists and removes rows with non-null 'key' before processing.
    """
    logger.info(f"Processing CSV file: {csv_file_path}")
    
    # Ensure the 'key' column exists in the table
    with conn.cursor() as cursor:
        cursor.execute("""
            DO $$
            BEGIN
                IF NOT EXISTS (
                    SELECT 1 
                    FROM information_schema.columns
                    WHERE table_name = 'indicateur_representation_details' 
                      AND column_name = 'key'
                ) THEN
                    ALTER TABLE meta.indicateur_representation_details
                    ADD COLUMN key TEXT;
                END IF;
            END
            $$;
        """)
        conn.commit()
        logger.info("'key' column checked/added in meta.indicateur_representation_details.")

    # Delete rows where 'key' is not NULL to avoid duplication and conflict
    with conn.cursor() as cursor:
        cursor.execute("""
            DELETE FROM meta.indicateur_representation_details
            WHERE key IS NOT NULL;
        """)
        conn.commit()
        logger.info("Deleted rows with non-null 'key' from meta.indicateur_representation_details.")
    
    # Read the CSV file
    with open(csv_file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        rows = [row for row in reader]

    logger.info(f"Read {len(rows)} rows from the CSV file.")
    
    with conn.cursor() as cursor:
        # Fetch the key-to-id mapping from meta.indicateur
        cursor.execute("SELECT key, id FROM meta.indicateur WHERE key IS NOT NULL;")
        key_to_id = {row[0]: row[1] for row in cursor.fetchall()}
        logger.info(f"Fetched {len(key_to_id)} key-to-id mappings from meta.indicateur.")

        # Process each row and insert into meta.indicateur_representation_details
        for row in rows:
            key = row['key']
            name = row['name']
            value = row['value']

            # Replace keys in the 'name' column
            if '#' in name:
                parts = name.split('#')
                base_name = parts[0]
                name_key = parts[1] if len(parts) > 1 else None
                name_id = key_to_id.get(name_key)
                if name_id:
                    name = f"{base_name}#{name_id}"
                else:
                    logger.warning(f"Name key '{name_key}' not found in meta.indicateur. Keeping original name.")
            
            # Get the 'id' for the main 'key'
            indicateur_id = key_to_id.get(key)
            if indicateur_id is None:
                logger.warning(f"Key '{key}' not found in meta.indicateur. Skipping row.")
                continue

            logger.info(f"Inserting row: key={key}, name={name}, value={value}, indicateur_id={indicateur_id}")
            cursor.execute("""
                INSERT INTO meta.indicateur_representation_details (indicateur_id, key, name, value)
                VALUES (%s, %s, %s, %s);
            """, (indicateur_id, key, name, value))
        
        conn.commit()
        logger.info("All valid rows have been inserted into meta.indicateur_representation_details.")


def insert_chart_from_csv(conn, csv_file_path):
    """
    Inserts data into the meta.chart table from the csv file.
    Links 'key' in the CSV to 'id' in meta.indicateur, and inserts data into meta.chart.
    Ensures 'key' column exists in meta.chart and deletes rows with non-null 'key' before insertion.
    """
    logger.info(f"Processing CSV file: {csv_file_path}")

    # Ensure the 'key' column exists in meta.chart
    with conn.cursor() as cursor:
        cursor.execute("""
            DO $$
            BEGIN
                IF NOT EXISTS (
                    SELECT 1
                    FROM information_schema.columns
                    WHERE table_name = 'chart' AND table_schema = 'meta' AND column_name = 'key'
                ) THEN
                    ALTER TABLE meta.chart ADD COLUMN key TEXT;
                END IF;
            END
            $$;
        """)
        conn.commit()
        logger.info("'key' column checked/added in meta.chart.")

    # Delete rows where 'key' is not NULL
    with conn.cursor() as cursor:
        cursor.execute("DELETE FROM meta.chart WHERE key IS NOT NULL;")
        conn.commit()
        logger.info("Deleted rows with non-null 'key' from meta.chart.")

    # Read the CSV file
    with open(csv_file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        rows = [row for row in reader]
    logger.info(f"Read {len(rows)} rows from the CSV file.")

    # Fetch the key-to-id mapping from meta.indicateur
    with conn.cursor() as cursor:
        cursor.execute("SELECT key, id FROM meta.indicateur WHERE key IS NOT NULL;")
        key_to_id = {row[0]: row[1] for row in cursor.fetchall()}
    logger.info(f"Fetched {len(key_to_id)} key-to-id mappings from meta.indicateur.")

    # Insert data into meta.chart
    with conn.cursor() as cursor:
        for row in rows:
            key = row['key']
            titre = row['titre']
            chart_type = row['type']
            categorie = row['categorie']
            visible = row['visible']
            ordre = row['ordre'] or None 
            data_type = row['data_type'] or None

            id_indicateur = key_to_id.get(key)
            if id_indicateur is None:
                logger.warning(f"Key '{key}' not found in meta.indicateur. Skipping row.")
                continue

            logger.info(f"Inserting row: key={key}, titre={titre}, type={chart_type}, id_indicateur={id_indicateur}")
            cursor.execute("""
                INSERT INTO meta.chart (indicateur, key, titre, type, categorie, visible, ordre, data_type)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
            """, (id_indicateur, key, titre, chart_type, categorie, visible, ordre, data_type))
        conn.commit()
        logger.info("All valid rows have been inserted into meta.chart.")


def update_concatenated_analysis(conn, analysis_key, column_name, keys_to_lookup):
    """
    Retrieve the ids for specified keys from meta.indicateur,
    then update the specified analysis_key column with a concatenation of these ids.
    
    analysis_key: The key in meta.indicateur to update
    column_name: The column in meta.indicateur to update with the concatenated ids
    keys_to_lookup: List of keys to look for in the database
    """

    # Get the ids for the specified keys
    analysis_ids = {}
    with conn.cursor() as cursor:
        for key in keys_to_lookup:
            logger.info(f"Looking up id for key: {key}")
            cursor.execute("SELECT id FROM meta.indicateur WHERE key = %s", (key,))
            result = cursor.fetchone()
            if result:
                analysis_ids[key] = result[0]
                logger.info(f"Found id {result[0]} for key: {key}")
            else:
                logger.warning(f"No id found for key: {key}")

    # Construct the new value for the column
    ids_list = list(analysis_ids.values())
    ids_string = '{' + ', '.join(map(str, ids_list)) + '}'
    logger.info(f"Constructed concatenated ids string: {ids_string}")

    # Find the row where key is analysis_key
    with conn.cursor() as cursor:
        cursor.execute("SELECT id FROM meta.indicateur WHERE key = %s", (analysis_key,))
        row = cursor.fetchone()
        if row:
            logger.info(f"Updating row with id {row[0]} for key: {analysis_key}")
            # Update the row
            cursor.execute("""
                UPDATE meta.indicateur
                SET {} = %s 
                WHERE id = %s
                """.format(column_name, column_name), (ids_string, row[0]))
            logger.info(f"Updated {column_name} for key: {analysis_key} with ids {ids_string}")
        else:
            logger.warning(f"No row found for key: {analysis_key}")

    # Commit the changes
    conn.commit()
    logger.info("Changes committed successfully.")


def main(config_file):
    """
    Main function to load the config and execute the SQL scripts.
    """
    logger.info(f"**********Fill csv files from database**********")
    logger.info(f"Loading configuration from file: {config_file}")
    with open(config_file, "r") as f:
        config = json.load(f)
    logger.info("Configuration loaded successfully.")

    conn = psycopg2.connect(**config["db_params"])
    user_params = config["user_params"]
    env_path = config["env_path"]
    dashboard_params = config["dashboard_params"]

    try:
        # Step 1: Create user if not exists
        logger.info("Step 1: Ensuring user exists...")
        create_user_if_not_exists(conn, env_path, user_params)

        # Step 2: Ensure 'key' column exists in meta.indicateur table
        logger.info("Step 2: Ensuring 'key' column exists in 'meta.indicateur'...")
        alter_table_add_column_if_needed(conn)

        # Step 3: INSERT analysis to meta.indicateur
        logger.info("Step 3: Executing SCRIPT_INSERT_INDICATEUR...")
        execute_sql_script_from_file(
            conn,
            "sql/SCRIPT_INSERT_INDICATEUR.sql"
        )

        # Step 4: UPDATE concatenation column in meta.indicateur
        logger.info("Step 4: UPDATE concatenation column in meta.indicateur...")
        update_concatenated_analysis(conn, 'polluants-atmo', 'concatenation', [
             'air_polluant_sox', 'air_polluant_nh3', 'air_polluant_pm10',
            'air_polluant_nox', 'air_polluant_covnm', 'air_polluant_pm25'
        ])

        update_concatenated_analysis(conn, 'part-enr-conso-ener', 'concatenation', [
            'conso-ener', 'prod-enr'
        ])

        # Step 5: Insert data into meta.indicateur_representation_details from CSV
        logger.info("Step 5: Inserting data into meta.indicateur_representation_details...")
        insert_indicateur_details_from_csv(conn, "csv/indicateur_details.csv")

        # Step 6: Insert or update categories into meta.categorie from csv
        logger.info("Step 6: Inserting data into meta.categorie...")
        update_or_insert_categorie(conn, "csv/categorie.csv")

        # Step 7: Insert or update into meta.chart from csv
        logger.info("Step 7: Inserting data into meta.chart...")
        insert_chart_from_csv(conn, "csv/chart.csv")

        # Step 8 : Create missing data tables in regional schema
        logger.info("Step 8: Creating missing data table in regional schema ...")
        execute_sql_script_from_file(
            conn,
            "sql/SCRIPT_CREATE_MISSING_TABLES.sql",
        )
        
        # Step 9: Check if dashboard exists or create it
        logger.info("Step 9: Checking if tableau exists or creating it...")
        dashboard_id = create_or_get_tableau_id(conn, dashboard_params, user_params)

        if dashboard_id:
            logger.info(f"Dashboard created successfully with ID: {dashboard_id}")

            # Step 10: Update "graphiques" with the appropriate ids from meta.indicateur and insert data into tableau_thematique
            logger.info("Step 10: Updating graphiques and inserting into meta.tableau_thematique...")
            update_thematique_and_insert(conn, dashboard_id, "json/rows_thematic.json")

    finally:
        logger.info("Closing the database connection.")
        conn.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="TerriSTORY® territorial file restruct database script")
    parser.add_argument(
        "-c", "--config", type=str, help="Config file", default="config.json"
    )
    args = parser.parse_args()
    main(args.config)