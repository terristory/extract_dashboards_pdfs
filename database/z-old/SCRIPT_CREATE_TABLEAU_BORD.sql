-- Start the transaction

-- Insert into tableau_bord 
INSERT INTO meta.tableau_bord (titre, mail, description, region, date, full_share, readonly_share)
VALUES (
    'Tableau de bord - territoire - test',
    '${mail}',
    'Ce tableau de bord est extrait de TerriSTORY®, outil partenarial d''aide au pilotage de la transition énergétique et écologique, co-construit avec les territoires par *Auvergne-Rhône-Alpes Énergie Environnement*. Il présente une sélection d''indicateurs clés utiles pour l''élaboration et le suivi des politiques de transition. Pour en savoir plus sur votre territoire, RV sur [terristory.fr](https://auvergnerhonealpes.terristory.fr) et, pour tout renseignement complémentaire, écrivez à terristory@auvergnerhonealpes.terristory.fr.',
    'auvergne-rhone-alpes',
    '2023-03-01 10:22:39.784389',
    '{}',
    '{}'
)
RETURNING id;
