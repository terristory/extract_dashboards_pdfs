import requests
import pandas as pd
import datetime
import json
dateparse = lambda x: datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f')

# Get the type of territory from config.json
with open("config.json", "r") as f:
    config = json.load(f)
    
TYPE = config["territory_type"]  # Change this to "departement" or "epci" as needed in config.json

# List of valid types
valid_types = ["epci", "departement", "scot"]

# Check if the TYPE is valid
if TYPE not in valid_types:
    raise ValueError("Invalid type of territory")

# Dynamically construct the file name for territories list
territories_file = f"{config['directory_path']}/inputs/csv/{TYPE}.csv"

# Load territories list
territories = pd.read_csv(territories_file, sep=";")


vals = []
for index, territory in territories.iterrows():
    territory_name = territory.nom
    siren = territory.code
    url = f"http://localhost:8080/api/auvergne-rhone-alpes/analysis/23/graphique/jauge-circulaire?zone={TYPE}&maille=commune&zone_id={siren}"
    body = "{}"

    rep = requests.post(url, body)
    vals.append({"territory": siren, "val": float(rep.content)})
df = pd.DataFrame.from_dict(vals)
df["date"] = datetime.datetime.now()
df["date"] = pd.to_datetime(df["date"])
df["territory"] = df["territory"].astype(str)
df_existing = pd.read_csv(f"{config['directory_path']}/inputs/csv/prod_enr_rate.csv", sep=";")
df_existing["date"] = pd.to_datetime(df_existing["date"])
df_existing["territory"] = df_existing["territory"].astype(str)
df = pd.concat([df, df_existing.astype(df.dtypes)], axis=0)
df = df.sort_values(by="date", ascending=False)
df.drop_duplicates(subset=["territory"], inplace=True)
df = df.sort_values(by="territory", ascending=True)
df.to_csv(f"{config['directory_path']}/inputs/csv/prod_enr_rate.csv", sep=";", index=False)
