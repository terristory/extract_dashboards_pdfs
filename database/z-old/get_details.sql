\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.epci e LEFT JOIN auvergne_rhone_alpes.prod_enr_methanisation m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/methanisation_epci.csv CSV HEADER DELIMITER ';';

\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.epci e LEFT JOIN auvergne_rhone_alpes.prod_enr_pv m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/pv_epci.csv CSV HEADER DELIMITER ';';
\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.epci e LEFT JOIN auvergne_rhone_alpes.prod_enr_bois m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/bois_epci.csv CSV HEADER DELIMITER ';';







\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.departement e LEFT JOIN auvergne_rhone_alpes.prod_enr_pv m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/pv_departements.csv CSV HEADER DELIMITER ';';
\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.departement e LEFT JOIN auvergne_rhone_alpes.prod_enr_methanisation m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/methanisation_departements.csv CSV HEADER DELIMITER ';';





\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.scot e LEFT JOIN auvergne_rhone_alpes.prod_enr_pv m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/pv_scots.csv CSV HEADER DELIMITER ';';
\copy (SELECT e.code, COALESCE(m.annee, 0) AS annees, COALESCE(SUM(m.valeur), 0) as somme FROM auvergne_rhone_alpes.scot e LEFT JOIN auvergne_rhone_alpes.prod_enr_methanisation m ON m.commune = ANY(e.communes) AND m.annee IN ('2015', '2019', '2020', '2021', '2022') GROUP BY e.code, m.annee ORDER BY e.code, m.annee) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/methanisation_scots.csv CSV HEADER DELIMITER ';';



\copy (SELECT nom, code, population, superficie, population/superficie as densite FROM auvergne_rhone_alpes.scot) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/scot.csv CSV HEADER DELIMITER ';';


-- GET EPCI ( or any other maille (scot, dept, ETC....)), population sum , densite sum for communes within the same EPCI
\copy 
( SELECT 
    REGEXP_REPLACE(
        REGEXP_REPLACE(e.nom::TEXT, '(^| )CC( |$)', ' Communauté de communes '),
        '(^| )CA( |$)', ' Communauté d''agglomération '
    ) AS nom, 
    e.code,
    SUM(p.valeur) AS population,  
    SUM(c.superficie) AS superficie,
    SUM(p.valeur) / NULLIF(SUM(s.valeur), 0) AS densite,
    CASE
        WHEN e.nom ILIKE 'CC%' THEN 'Communauté de communes'
        WHEN e.nom ILIKE 'CA%' THEN 'Communauté d''agglomération'
        ELSE NULL
    END AS first_line,
    TRIM(
        REGEXP_REPLACE(
            REGEXP_REPLACE(
                e.nom::TEXT, 
                '(^| )CC( |$)', ' ' 
            ), 
            '(^| )CA( |$)', ' '
        )
    ) AS second_line
FROM 
    auvergne_rhone_alpes.epci e
JOIN 
    auvergne_rhone_alpes.territoire t 
    ON e.code = t.code
    AND t.type_territoire = 'epci'
JOIN 
    auvergne_rhone_alpes.commune c
    ON t.commune = c.code
JOIN 
    auvergne_rhone_alpes.population p 
    ON c.code = p.commune
    AND t.code = e.code
    AND p.annee = 2021
JOIN 
    auvergne_rhone_alpes.superficie_cadastrale s
    ON c.code = s.commune
    AND t.code = e.code
WHERE 
    e.nom NOT ILIKE '%partie AURA%'
GROUP BY 
    e.nom, 
    e.code
ORDER BY 
    e.nom

 ) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/epci.csv CSV HEADER DELIMITER ';';



-- departement 

\COPY 
(SELECT 
    d.nom, 
    d.code,
    SUM(p.valeur) AS population,  
    SUM(c.superficie) AS superficie,
    SUM(p.valeur) / NULLIF(SUM(s.valeur), 0) AS densite,
    d.nom as first_line,
    '' AS second_line
FROM 
    auvergne_rhone_alpes.departement d
JOIN 
    auvergne_rhone_alpes.territoire t 
    ON d.code = t.code
    AND t.type_territoire = 'departement'
JOIN 
    auvergne_rhone_alpes.commune c
    ON t.commune = c.code
JOIN 
    auvergne_rhone_alpes.population p 
    ON c.code = p.commune
    AND t.code = d.code
    AND p.annee = 2021
JOIN 
    auvergne_rhone_alpes.superficie_cadastrale s
    ON c.code = s.commune
    AND t.code = d.code
GROUP BY 
    d.nom, 
    d.code
ORDER BY 
    d.nom) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/departement.csv CSV HEADER DELIMITER ';';



-- SCOT

\COPY
(SELECT 
    d.nom, 
    d.code,
    SUM(p.valeur) AS population,  
    SUM(c.superficie) AS superficie,
    SUM(p.valeur) / NULLIF(SUM(s.valeur), 0) AS densite,
    split_part(d.nom, ' ', 1) AS first_line, 
    CASE
        WHEN length(d.nom) > length(split_part(d.nom, ' ', 1)) 
        THEN substring(d.nom FROM length(split_part(d.nom, ' ', 1)) + 2)
        ELSE ''
    END AS second_line
FROM 
    auvergne_rhone_alpes.scot d
JOIN 
    auvergne_rhone_alpes.territoire t 
    ON d.code = t.code
    AND t.type_territoire = 'scot'
JOIN 
    auvergne_rhone_alpes.commune c
    ON t.commune = c.code
JOIN 
    auvergne_rhone_alpes.population p 
    ON c.code = p.commune
    AND t.code = d.code
    AND p.annee = 2021
JOIN 
    auvergne_rhone_alpes.superficie_cadastrale s
    ON c.code = s.commune
    AND t.code = d.code
GROUP BY 
    d.nom, 
    d.code
ORDER BY 
    d.nom) TO /home/ffattouh/extract_dashboards_pdfs/inputs/csv/scot.csv CSV HEADER DELIMITER ';';