-- Start the transaction
-- Insert into meta.tableau_thematique

INSERT INTO meta.tableau_thematique (titre, tableau, graphiques, description, ordre)
VALUES
    ('Profil du territoire', '${returned_id}', 
    '[{"numero_analyse": "1", "id_analysis": 203, "representation": "marqueur-svg", "tailleSVG": 4, "tailleData": 3, "imageSVG": "MdGroups", "categories": [], "key": "population"}, 
      {"numero_analyse": "2", "id_analysis": 204, "representation": "marqueur-svg", "tailleSVG": 4, "tailleData": 3, "imageSVG": "MdGroups", "categories": [], "key": "densite-population"}, 
      {"numero_analyse": "29", "id_analysis": 36, "representation": "pie", "categories": {"secteur_clap17": {"categorie": "secteur_clap17", "titre": "Répartition en 17 secteurs", "visible": false}, "secteur_clap5": {"categorie": "secteur_clap5", "titre": "Répartition en 5 secteurs", "visible": true}}, "key": "emplois-salarie-secteur"}, 
      {"numero_analyse": "31", "id_analysis": 19, "representation": "bar-years", "categories": {"secteur": {"categorie": "secteur", "titre": "Par secteurs", "visible": false}, "energie": {"categorie": "energie", "titre": "Par type d''énergie", "visible": true}, "usage": {"categorie": "usage", "titre": "Par usages", "visible": false}}, "key": "facture-ener"}, 
      {"numero_analyse": "32", "id_analysis": 23, "representation": "courbes_croisees", "categories": {"type_prod_enr": {"categorie": "type_prod_enr", "titre": "Par filières de production", "visible": false}}, "key": "part-enr-conso-ener"}]', 
    'Pour une vision socio-économique synthétique du territoire sur la base d''indicateurs relatifs à la population, aux emplois et à la consommation d''énergie.', 
    0),

    ('Énergie', '${returned_id}', 
    '[{"numero_analyse": "4", "id_analysis": 1, "representation": "bar-years", "categories": {"secteur": {"categorie": "secteur", "titre": "Par secteur", "visible": true}, "energie": {"categorie": "energie", "titre": "Par type d''énergie", "visible": false}, "usage": {"categorie": "usage", "titre": "Par usage", "visible": false}}, "key": "conso-ener"}, 
      {"numero_analyse": "5", "id_analysis": 143, "representation": "pie", "categories": {"type_logement": {"categorie": "type_logement", "titre": "Par type de logement", "visible": false}, "type_cmbl": {"categorie": "type_cmbl", "titre": "Par système de chauffage", "visible": false}, "et_ener": {"categorie": "et_ener", "titre": "Par étiquette énergétique", "visible": true}}, "key": "logement-ener"}, 
      {"numero_analyse": "6", "id_analysis": 75, "representation": "pie", "categories": {"csp": {"categorie": "csp", "titre": "Par catégorie socio-professionnelle", "visible": false}, "type_menage": {"categorie": "type_menage", "titre": "Par type de ménage", "visible": false}, "trans_ppal": {"categorie": "trans_ppal", "titre": "Par type de transport", "visible": true}}, "key": "domicile-travail"}, 
      {"numero_analyse": "18", "id_analysis": 196, "representation": "marqueur-svg", "tailleSVG": 4, "tailleData": 3, "imageSVG": "MdThermostat", "categories": [], "key": "besoins-indus-chaleur"}]', 
    'Grand angle sur la consommation d''énergie du territoire et mise en lumière d''enjeux relatifs à l''efficacité énergétique des bâtiments, la mobilité et les besoins industriels en chaleur.', 
    1),

    ('Production d''énergies renouvelables', '${returned_id}', 
    '[{"numero_analyse": "12", "id_analysis": 46, "representation": "bar-years", "categories": {"type_prod_enr": {"categorie": "type_prod_enr", "titre": "Par filière de production", "visible": false}}, "key": "prod-enr"}, 
      {"numero_analyse": "14", "id_analysis": 28, "representation": "pie", "categories": {"type_photovoltaique": {"categorie": "type_photovoltaique", "titre": "Par type de bâtiment et parking", "visible": true}, "orientation_photovoltaique": {"categorie": "orientation_photovoltaique", "titre": "Orientation", "visible": false}, "contrainte_photovoltaique": {"categorie": "contrainte_photovoltaique", "titre": "Contraintes patrimoniales", "visible": false}}, "key": "potentiel-solaire-pv"}, 
      {"numero_analyse": "15", "id_analysis": 24, "representation": "pie", "categories": {"utilisation_potentiel": {"categorie": "utilisation_potentiel", "titre": "Utilisation du potentiel", "visible": false}, "filiere_methanisation": {"categorie": "filiere_methanisation", "titre": "Par type de ressource", "visible": true}}, "key": "potentiel-metha"}, 
      {"numero_analyse": "34", "id_analysis": 15122, "representation": "bar-years", "categories": {"type_prod_enr_pv": {"categorie": "type_prod_enr_pv", "titre": "Filière de production", "visible": false}}, "key": "prod-pv"}, 
      {"numero_analyse": "36", "id_analysis": 15123, "representation": "bar-years", "categories": {"type_prod_enr": {"categorie": "type_prod_enr", "titre": "Filière de production", "visible": false}}, "key": "prod-metha"}, 
      {"numero_analyse": "38", "id_analysis": 15124, "representation": "bar-years", "categories": {"type_prod_enr_bois": {"categorie": "type_prod_enr_bois", "titre": "Filière de production", "visible": false}}, "key": "prod-bois-biomasse"}]', 
    'Zoom sur la production d''EnR et le potentiel de production de la filière photovoltaïque et de la méthanisation.', 
    2),

    ('Émissions de gaz à effet de serre', '${returned_id}', 
    '[{"numero_analyse": "20", "id_analysis": 29, "representation": "bar-years", "categories": {"secteur": {"categorie": "secteur", "titre": "Par secteur", "visible": true}, "energie": {"categorie": "energie", "titre": "Par type d''énergie", "visible": false}, "usage": {"categorie": "usage", "titre": "Par usage", "visible": false}}, "key": "emission-ges"}, 
      {"numero_analyse": "21", "id_analysis": 29, "representation": "bar-years", "categories": {"secteur": {"categorie": "secteur", "titre": "Par secteur", "visible": false}, "energie": {"categorie": "energie", "titre": "Par type d''énergie", "visible": true}, "usage": {"categorie": "usage", "titre": "Par usage", "visible": false}}, "key": "emission-ges"}, 
      {"numero_analyse": "22", "id_analysis": 26, "representation": "pie", "categories": {"essence": {"categorie": "essence", "titre": "Type d''essence", "visible": true}}, "key": "potentiel-bois"}]', 
    'Gros plan sur les émissions de GES par secteur et type d''énergie, et sur les stocks de carbone par type de surface.', 
    3),

    ('Pollution atmosphérique', '${returned_id}', 
    '[{"numero_analyse": "39", "id_analysis": 102, "representation": "histogramme-normalise", "categories": [], "key": "polluants-atmo"}, 
      {"numero_analyse": "41", "options": {"enableEvolution": true}, "id_analysis": 102, "representation": "courbes-historiques", "categories": [], "key": "polluants-atmo"}]', 
    'Indicateurs sur les polluants', 
    4);
