CREATE TABLE auvergne_rhone_alpes.prod_enr_pv 
AS SELECT
    annee, type_prod_enr as type_prod_enr_pv, valeur, commune
FROM
    auvergne_rhone_alpes.prod_enr
WHERE
    type_prod_enr = ANY(
        SELECT
            modalite_id
        FROM
            meta.categorie
        WHERE
            nom = 'type_prod_enr'
            AND region LIKE 'auv%'
            AND modalite IN ('Production photovoltaïque', 'Production du solaire thermique')
    );
INSERT INTO meta.indicateur (nom, data, type, color_start, color_end, unit, active, years, ui_theme, decimals, region) 
    VALUES('Production photovoltaïque', 'prod_enr_pv', 'circle', '#007f7F', '#007f7F', 'GWh', true, '{2022,2021,2020,2019,2015}', 'Production d''énergie', 2, 'auvergne-rhone-alpes');
INSERT INTO meta.chart VALUES((SELECT id FROM meta.indicateur WHERE data = 'prod_enr_pv' AND region LIKE 'auv%'), 'Filière de production', 'pie', 'type_prod_enr_pv', true, NULL, NULL);
INSERT INTO meta.categorie (
        SELECT
            'type_prod_enr_pv', modalite, modalite_id, CASE WHEN modalite = 'Production photovoltaïque' THEN '#e3dd00' ELSE '#ee7f00' END AS couleur, ordre, region
        FROM
            meta.categorie
        WHERE
            nom = 'type_prod_enr'
            AND region LIKE 'auv%'
            AND modalite IN ('Production photovoltaïque', 'Production du solaire thermique')
    );


CREATE TABLE auvergne_rhone_alpes.prod_enr_methanisation 
AS SELECT
    annee, type_prod_enr, valeur, commune
FROM
    auvergne_rhone_alpes.prod_enr
WHERE
    type_prod_enr = ANY(
        SELECT
            modalite_id
        FROM
            meta.categorie
        WHERE
            nom = 'type_prod_enr'
            AND region LIKE 'auv%'
            AND modalite IN ('Valorisation électrique du biogaz', 'Valorisation thermique du biogaz', 'Valorisation par injection de biométhane')
    );
INSERT INTO meta.indicateur (nom, data, type, color_start, color_end, unit, active, years, ui_theme, decimals, region) 
    VALUES('Production méthanisation', 'prod_enr_methanisation', 'circle', '#007f7F', '#007f7F', 'GWh', true, '{2022,2021,2020,2019,2015}', 'Production d''énergie', 2, 'auvergne-rhone-alpes');
INSERT INTO meta.chart VALUES((SELECT id FROM meta.indicateur WHERE data = 'prod_enr_methanisation' AND region LIKE 'auv%'), 'Filière de production', 'pie', 'type_prod_enr', true, NULL, NULL);
-- INSERT INTO meta.categorie (
--         SELECT
--             'type_prod_enr', modalite, modalite_id, couleur, ordre, region
--         FROM
--             meta.categorie
--         WHERE
--             nom = 'type_prod_enr'
--             AND region LIKE 'auv%'
--             AND modalite IN ('Valorisation électrique du biogaz', 'Valorisation thermique du biogaz', 'Valorisation par injection de biométhane')
--     );


CREATE TABLE auvergne_rhone_alpes.prod_enr_bois
AS SELECT
    annee, type_prod_enr AS type_prod_enr_bois, valeur, commune
FROM
    auvergne_rhone_alpes.prod_enr
WHERE
    type_prod_enr = ANY(
        SELECT
            modalite_id
        FROM
            meta.categorie
        WHERE
            nom = 'type_prod_enr'
            AND region LIKE 'auv%'
            AND modalite = 'Valorisation du bois et autres biomasses solides'
    );
INSERT INTO meta.indicateur (nom, data, type, color_start, color_end, unit, active, years, ui_theme, decimals, region) 
    VALUES('Production bois/biomasse', 'prod_enr_bois', 'circle', '#007f7F', '#007f7F', 'GWh', true, '{2022,2021,2020,2019,2015}', 'Production d''énergie', 2, 'auvergne-rhone-alpes');
INSERT INTO meta.chart VALUES((SELECT id FROM meta.indicateur WHERE data = 'prod_enr_bois' AND region LIKE 'auv%'), 'Filière de production', 'pie', 'type_prod_enr_bois', true, NULL, NULL);
INSERT INTO meta.categorie (
        SELECT
            'type_prod_enr_bois', modalite, modalite_id, '#e3dd00', ordre, region
        FROM
            meta.categorie
        WHERE
            nom = 'type_prod_enr'
            AND region LIKE 'auv%'
            AND modalite = 'Valorisation du bois et autres biomasses solides'
    );



UPDATE meta.categorie SET couleur = '#e32d61' WHERE modalite = 'Production nette des PAC aérothermique' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#7d0426' WHERE modalite = 'Production nette des PAC géothermique' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#91d4f2' WHERE modalite = 'Production éolienne' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#21a5ff' WHERE modalite = 'Production hydroélectrique - puiss inf 4,5 MW' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#006ab0' WHERE modalite = 'Production hydroélectrique - puiss sup 4,5 MW' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#933588' WHERE modalite = 'Valorisation électrique des déchets' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#491b44' WHERE modalite = 'Valorisation thermique des déchets' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#908958' WHERE modalite = 'Valorisation électrique du biogaz' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#0f5117' WHERE modalite = 'Valorisation thermique du biogaz' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#cfd1d2' WHERE modalite = 'Autre valorisation électrique renouvelable' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#fecf44' WHERE modalite = 'Production photovoltaïque' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#a74d47' WHERE modalite = 'Valorisation du bois et autres biomasses solides' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#73D511' WHERE modalite = 'Valorisation par injection de biométhane' AND nom = 'type_prod_enr' AND region LIKE 'auv%';
UPDATE meta.categorie SET couleur = '#f29400' WHERE modalite = 'Production du solaire thermique' AND nom = 'type_prod_enr' AND region LIKE 'auv%';


SELECT id, data, years, unit FROM meta.indicateur WHERE id IN (1, 102, 143, 19, 196, 203, 204, 24, 26, 28, 29, 36, 46, 6278, 6279, 6280, 7177, 75) ORDER BY id;

UPDATE meta.indicateur SET years = '{2022,2021,2020,2015}', unit = 'GWh' WHERE id = 1;
UPDATE meta.indicateur SET years = '{2021,2020,2019,2018,2017,2016,2015}', unit = 'k€' WHERE id = 19;
UPDATE meta.indicateur SET years = '{2022,2021,2020,2019,2018,2017,2016,2015}', data = 'potentiel_methanisation*0.001', unit = 'GWh' WHERE id = 24;
UPDATE meta.indicateur SET years = '{2022}', unit = 'ha' WHERE id = 26;
UPDATE meta.indicateur SET years = '{2019}', unit = 'GWh' WHERE id = 28;
UPDATE meta.indicateur SET years = '{2022,2021,2020,2015}', unit = 'kt eq. CO₂' WHERE id = 29;
UPDATE meta.indicateur SET years = '{2019}', unit = 'emplois' WHERE id = 36;
UPDATE meta.indicateur SET years = '{2022,2021,2020,2015}', unit = 'GWh' WHERE id = 46;
UPDATE meta.indicateur SET years = '{2017}', unit = 'personnes' WHERE id = 75;
UPDATE meta.indicateur SET years = '{2022,2021,2020,2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005}', unit = 't-eq. CO2' WHERE id = 102;
UPDATE meta.indicateur SET years = '{2019}', unit = 'logements' WHERE id = 143;
UPDATE meta.indicateur SET years = '{2018}', unit = 'GWh' WHERE id = 196;
UPDATE meta.indicateur SET years = '{2020}', unit = 'hab.' WHERE id = 203;
UPDATE meta.indicateur SET years = '{2020}', unit = 'hab./km²' WHERE id = 204;
UPDATE meta.indicateur SET years = '{2022,2021,2020,2019,2015}', unit = 'GWh' WHERE id = (SELECT id FROM meta.indicateur WHERE data = 'prod_enr_pv' AND region LIKE 'auv%');
UPDATE meta.indicateur SET years = '{2022,2021,2020,2019,2015}', unit = 'GWh' WHERE id = (SELECT id FROM meta.indicateur WHERE data = 'prod_enr_methanisation' AND region LIKE 'auv%');
UPDATE meta.indicateur SET years = '{2022,2021,2020,2019,2015}', unit = 'GWh' WHERE id = (SELECT id FROM meta.indicateur WHERE data = 'prod_enr_bois' AND region LIKE 'auv%');
-- USED ?
UPDATE meta.indicateur SET years = '{2022,2021,2020,2019,2018,2017,2016,2015}', unit = 'Gwh' WHERE id = 7177;



-- INSERT INTO meta.indicateur (nom, data, type, color_start, color_end, unit, active, years, ui_theme, decimals, region, concatenation) 
--     VALUES('Part de production EnR/Consommation d''énergie', 'part_prod_enr', 'circle', '#007f7F', '#007f7F', 'GWh', true, 
--     '{2022,2021,2020,2019,2018,2017,2016,2015}', 'Production d''énergie', 2, 'auvergne-rhone-alpes', '{1,46}');



UPDATE meta.categorie SET modalite = 'Transport routier', couleur='#333e4d' WHERE modalite_id = 6 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Agriculture, sylviculture et aquaculture', couleur='#007d75' WHERE modalite_id = 2 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Autres transports', couleur='#6c84a3' WHERE modalite_id = 7 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Industrie hors branche énergie', couleur='#ee7f00' WHERE modalite_id = 5 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Tertiaire', couleur='#63bee1' WHERE modalite_id = 3 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Gestion des déchets', couleur='#933588' WHERE modalite_id = 4 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Résidentiel', couleur='#e3dd00' WHERE modalite_id = 1 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';
UPDATE meta.categorie SET modalite = 'Industrie branche énergie', couleur='#e85444' WHERE modalite_id = 8 AND region = 'auvergne-rhone-alpes' AND nom = 'secteur';



--- polluants id = 102 and part_enr/conso id = 23----

\copy meta.indicateur_representation_details from '/home/ffattouh/data/territorialFile/indicateur_details.csv' csv header delimiter ';'