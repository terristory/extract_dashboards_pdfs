import os
from PyPDF2 import PdfMerger

def combine_pdfs(pdf_folder, output_file):
    # Create a PdfMerger object
    merger = PdfMerger()

    # Get all PDF files in the folder
    pdf_files = [f for f in os.listdir(pdf_folder) if f.endswith('.pdf')]
    pdf_files.sort()

    # Add each PDF to the merger
    for pdf in pdf_files:
        pdf_path = os.path.join(pdf_folder, pdf)
        merger.append(pdf_path)

    # Write the combined PDF to the output file
    merger.write(output_file)
    merger.close()

    print(f"Combined {len(pdf_files)} PDFs into {output_file}")

# Merge Files
pdf_folder = 'outputs/steps/6_doublepage'
output_file = 'outputs/steps/7_merged_pdfs/merged_pdfs.pdf' 

combine_pdfs(pdf_folder, output_file)
