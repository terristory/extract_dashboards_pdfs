import fitz
from fitz.utils import getColor, Colorspace, CS_CMYK
import slugify
import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import json

# Get the type of territory from config.json
with open("config.json", "r") as f:
    config = json.load(f)
    
TYPE = config["territory_type"]  # Change this to "departement" or "epci" as needed in config.json

# List of valid types
valid_types = ["epci", "departement", "scot"]

# Check if the TYPE is valid
if TYPE not in valid_types:
    raise ValueError("Invalid type of territory")

# Dynamically construct the file name for territories list
territories_file = f"inputs/csv/{TYPE}.csv"

# Load territories list
territories_list = pd.read_csv(territories_file, sep=";", dtype={'code': str})

prod_enr_rate = pd.read_csv("inputs/csv/prod_enr_rate.csv", sep=";")

# Dynamically construct the file name for methanization
methanization_file = f"inputs/csv/methanisation_{TYPE}.csv"
methanization = pd.read_csv(methanization_file, sep=";")

src_pdf_filename = "templates/Livrets-Terristory_vierge.pdf"

for index, epci in territories_list.iterrows():
    epci_name = epci.nom
    siren = str(epci.code)

    dst_pdf_filename = f"outputs/steps/1_fonts/{siren}-{slugify.slugify(epci_name)}.pdf"
    print("siren", siren)
    print("dst_pdf_filename", dst_pdf_filename)

    # http://pymupdf.readthedocs.io/en/latest/rect/
    # Set position and size according to your needs
    document = fitz.open(src_pdf_filename)

    # We'll put image on first page only but you could put it elsewhere
    page = document[0]
    # page.clean_contents(False)

    width, height = page.mediabox_size

    fontsize_to_use = 24
    fontname_to_use = "Montserrat"
    rc = page.insert_font(
        fontname=fontname_to_use, fontfile="fonts/Montserrat-Bold.ttf"
    )

    rect_x1 = width * 0.1
    rect_x2 = width * 0.9
    if str(epci.second_line) == "nan":
        rect_y1 = height * 0.308
        rect_y2 = height * 0.45
        fontsize_to_use = 26
    elif len(epci.second_line) < 30:
        rect_y1 = height * 0.285
        rect_y2 = height * 0.41
    else:
        rect_y1 = height * 0.265
        rect_y2 = height * 0.4
    if TYPE == "departement":
        fontsize_to_use = 38
        rect_y1 = height * 0.300
    if TYPE == "departement" and siren == "69D":
        fontsize_to_use = 32
        rect_y1 = height * 0.300

    rect = (rect_x1, rect_y1, rect_x2, rect_y2)

    ## Uncomment if you wish to display rect
    # page.draw_rect(rect,color=(1,0.25,0.25))

    rc = page.insert_textbox(
        rect,
        epci.first_line,
        fontsize=fontsize_to_use,
        fontname=fontname_to_use,
        color=getColor("white"),
        align=1,
    )  ## Uncomment if you wish to display rect
    if rc <= 0:
        raise ValueError(f"Couldn't print epci.first_line for {TYPE} - {siren}")

    if str(epci.second_line) != "nan":
        rect_y1 = rect_y1 + 28 + 5
        rect_y2 = height * 0.7

        rect = (rect_x1, rect_y1, rect_x2, rect_y2)

        # page.draw_rect(rect,color=(.25,1,0.25))

        rc = page.insert_textbox(
            rect,
            epci.second_line,
            fontsize=24,
            fontname=fontname_to_use,
            color=getColor("white"),
            align=1,
        )
        if rc <= 0:
            raise ValueError(f"Couldn't print epci.second_line for {TYPE} - {siren}")

    # density
    page = document[2]
    rect_x1 = 20
    rect_x2 = 85.64 + 46 + rect_x1
    rect_y1 = 188.75
    rect_y2 = 60.10 + rect_y1
    rect = (rect_x1, rect_y1, rect_x2, rect_y2)

    fontname_to_use = "Montserrat-Light"
    rc = page.insert_font(
        fontname=fontname_to_use, fontfile="fonts/Montserrat-Light.ttf"
    )

    ## Uncomment if you wish to display rect
    # page.draw_rect(rect, color=(1,0.25,0.25))
    density = round(epci.densite, 2)
    if density > 10:
        density = round(density, 1)
        if density > 100:
            density = int(round(density, 0))
    rc = page.insert_textbox(
        rect,
        str("{:,}".format(density).replace(",", " ")).replace(".", ","),
        fontsize=36,
        fontname=fontname_to_use,
        color=(0.95, 0.25, 0.55, 0.07),
        align=2,
    )  ## Uncomment if you wish to display rect
    if rc <= 0:
        raise ValueError(f"Couldn't print density for {TYPE} - {siren}")

    fontname_to_use = "Montserrat"
    rc = page.insert_font(fontname=fontname_to_use, fontfile="fonts/Montserrat.ttf")
    rect_x1 = 160.64
    rect_x2 = 160.64 + rect_x1
    rect_y1 = 206
    rect_y2 = 60.10 + rect_y1
    rect = (rect_x1, rect_y1, rect_x2, rect_y2)
    ## Uncomment if you wish to display rect
    # page.draw_rect(rect, color=(1,0.25,0.25))

    rc = page.insert_textbox(
        rect,
        "hab./km²",
        fontsize=18,
        fontname=fontname_to_use,
        color=(0.95, 0.25, 0.55, 0.07),
        align=3,
    )  ## Uncomment if you wish to display rect
    if rc <= 0:
        raise ValueError(f"Couldn't print density unit for {TYPE} - {siren}")

    # population
    rect_x1 = 120
    rect_x2 = 320.64 + rect_x1
    rect_y1 = 188.75
    rect_y2 = 60.10 + rect_y1
    rect = (rect_x1, rect_y1, rect_x2, rect_y2)

    fontname_to_use = "Montserrat-Light"

    ## Uncomment if you wish to display rect
    # page.draw_rect(rect, color=(1,0.25,0.25))
    rc = page.insert_textbox(
        rect,
        "{:,}".format(epci.population).replace(",", " "),
        fontsize=36,
        fontname=fontname_to_use,
        color=(0.95, 0.25, 0.55, 0.07),
        align=2,
    )  ## Uncomment if you wish to display rect
    if rc <= 0:
        raise ValueError(f"Couldn't print population for {TYPE} - {siren}")

    rect_x1 = 449.64
    rect_x2 = 120.64 + rect_x1
    rect_y1 = 206
    rect_y2 = 60.10 + rect_y1
    rect = (rect_x1, rect_y1, rect_x2, rect_y2)
    ## Uncomment if you wish to display rect
    # page.draw_rect(rect, color=(1,0.25,0.25))
    fontname_to_use = "Montserrat"

    rc = page.insert_textbox(
        rect,
        "hab.",
        fontsize=18,
        fontname=fontname_to_use,
        color=(0.95, 0.25, 0.55, 0.07),
        align=3,
    )  ## Uncomment if you wish to display rect
    if rc <= 0:
        raise ValueError(f"Couldn't print population unit for {TYPE} - {siren}")

    # rate EnR
    page = document[3]

    fontname_to_use = "Montserrat-Light"
    rc = page.insert_font(
        fontname=fontname_to_use, fontfile="fonts/Montserrat-Light.ttf"
    )

    fontname_to_use = "Montserrat"
    rc = page.insert_font(fontname=fontname_to_use, fontfile="fonts/Montserrat.ttf")

    if prod_enr_rate["territory"].dtype == int:
        siren = int(siren)
    else: 
        siren = str(siren)
    val = round(prod_enr_rate.loc[prod_enr_rate["territory"] == siren, "val"].iloc[0], 2)
    val = int(round(val, 0))
    rect_x1 = 92
    rect_x2 = 138 + rect_x1
    rect_y1 = 175.75
    rect_y2 = 60.10 + rect_y1
    rect = (rect_x1, rect_y1, rect_x2, rect_y2)

    fontname_to_use = "Montserrat-Light"

    ## Uncomment if you wish to display rect
    # page.draw_rect(rect, color=(1,0.25,0.25))
    rc = page.insert_textbox(
        rect,
        str(val).replace(".", ",") + " %",
        fontsize=36,
        fontname=fontname_to_use,
        color=(0.95, 0.25, 0.55, 0.07),
        align=3,
    )  ## Uncomment if you wish to display rect
    if rc <= 0:
        raise ValueError(f"Couldn't print prod_enr_rate for {TYPE} - {siren}")

    # fontname_to_use = "Montserrat"
    # rect_x1 = 160.64
    # rect_x2 = 160.64+rect_x1
    # rect_y1 = 192
    # rect_y2 = 45.10+rect_y1
    # rect = (rect_x1, rect_y1, rect_x2, rect_y2)
    # ## Uncomment if you wish to display rect
    # page.draw_rect(rect, color=(1,0.25,0.25))


    # rc = page.insert_textbox(rect, "%",
    #                         fontsize=18,
    #                         fontname=fontname_to_use,
    #                         color=(0.95, 0.25, 0.55, 0.07),
    #                         align=3)    ## Uncomment if you wish to display rect

    # Ensure the type consistency for 'siren' based on the 'code' column data type
    if methanization["code"].dtype == int:
        siren = int(siren)
    else: 
        siren = str(siren)

    # Step 1: Find the last year
    last_year = methanization["annees"].max()

    # Step 2: Filter for the last year and the specific code
    val_methane = methanization.loc[
        (methanization["code"] == siren) & (methanization["annees"] == last_year), 
        "somme"
    ].sum()
    
    if val_methane == 0.0:
        print("\t Add methanization message")
        page = document[9]

        fontname_to_use = "Montserrat-Light"
        rc = page.insert_font(
            fontname=fontname_to_use, fontfile="fonts/Montserrat-Light.ttf"
        )

        fontname_to_use = "Montserrat"
        rc = page.insert_font(fontname=fontname_to_use, fontfile="fonts/Montserrat.ttf")

        rect_x1 = 92
        rect_x2 = 450 + rect_x1
        rect_y1 = 195.75
        rect_y2 = 250.10 + rect_y1
        rect = (rect_x1, rect_y1, rect_x2, rect_y2)

        fontname_to_use = "Montserrat-Light"

        ## Uncomment if you wish to display rect
        # page.draw_rect(rect, color=(1,0.25,0.25))
        rc = page.insert_textbox(
            rect,
            "Aucune production d'énergie à partir de méthanisation\nn'est disponible sur le territoire.",
            fontsize=13,
            fontname=fontname_to_use,
            color=(0.18, 0.21, 0.27),
            align=3,
        )  ## Uncomment if you wish to display rect
        if rc <= 0:
            raise ValueError(f"Couldn't print methanization empty message for {TYPE} - {siren}")

    # See http://pymupdf.readthedocs.io/en/latest/document/#Document.save and
    # http://pymupdf.readthedocs.io/en/latest/document/#Document.saveIncr for
    # additional parameters, especially if you want to overwrite existing PDF
    # instead of writing new PDF
    document.save(dst_pdf_filename)
    print("dst_pdf_filename", dst_pdf_filename)
    document.close()
