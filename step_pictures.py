import fitz
from fitz.utils import getColor
import os
import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import slugify
import json

# Get the type of territory from config.json
with open("config.json", "r") as f:
    config = json.load(f)
    
TYPE = config["territory_type"]  # Change this to "departement" or "epci" as needed in config.json

# List of valid types
valid_types = ["epci", "departement", "scot"]

# Check if the TYPE is valid
if TYPE not in valid_types:
    raise ValueError("Invalid type of territory")

# Dynamically construct the file name for territories list
territories_file = f"inputs/csv/{TYPE}.csv"

# Load territories list
territories_list = pd.read_csv(territories_file, sep=";", dtype={'code': str})

# Dynamically construct the file name for methanization
methanization_file = f"inputs/csv/methanisation_{TYPE}.csv"

# Load methanization
methanization = pd.read_csv(methanization_file, sep=";")

coordinates = pd.read_csv("inputs/csv/figures.csv", sep="\t")
GS_EXEC_NAME = "gs"


def get_picture_size(path):
    im = Image.open(path)
    return im.size


def convert_to_cmyk(path):
    im = Image.open(path)
    new_path = path.replace(".png", "_cmyk.jpg")
    im.convert("CMYK").save(new_path)
    im.close()
    return new_path


def crop_picture(path, left, right, top, bottom):
    im = Image.open(path)

    # Size of the image in pixels (size of original image)
    # (This is not mandatory)
    width, height = im.size

    # Cropped image of above dimension
    # (It will not change original image)
    im1 = im.crop((left, top, width - right, height - bottom))
    new_path = path.replace(".jpg", "_cropped.jpg").replace(".png", "_cropped.png")
    im1.save(new_path)
    im.close()
    return new_path


for index, epci in territories_list.iterrows():
    epci_name = epci.nom
    siren = str(epci.code)
    if not os.path.isdir(f"inputs/graphs/{siren}/"):
        continue
    print()
    print(index, "/", len(territories_list))
    print()
    print(siren)

    src_pdf_filename = f"outputs/steps/1_fonts/{siren}-{slugify.slugify(epci_name)}.pdf"
    dst_pdf_filename = (
        f"outputs/steps/2_pictures/{siren}-{slugify.slugify(epci_name)}.pdf"
    )
    compressed_dst_pdf_filename = (
        f"outputs/steps/3_pdf/{siren}-{slugify.slugify(epci_name)}.pdf"
    )

    # http://pymupdf.readthedocs.io/en/latest/rect/
    # Set position and size according to your needs
    document = fitz.open(src_pdf_filename)

    # We'll put image on first page only but you could put it elsewhere
    page = document[2]
    width, height = page.mediabox_size

    for _, coords in coordinates.iterrows():
        page = document[int(coords["page"]) - 1]
        width, height = page.mediabox_size
        print(f"\t Picture {coords.source}")

        img_filename = f"inputs/graphs/{siren}/{coords.source}.png"

        if coords.source == "13_production-methanisation":
            # Ensure the type consistency for 'siren' based on the 'code' column data type
            if methanization["code"].dtype == int:
                siren = int(epci.code)
            else:
                siren = str(epci.code)

            last_year = methanization["annees"].max()

            # Step 2: Filter for the last year and the specific code
            val_methane = methanization.loc[
                (methanization["code"] == siren) & (methanization["annees"] == last_year), 
                "somme"
            ].sum()

            if val_methane == 0.0:
                print("\t skip methanization")
                continue

        # img_filename = convert_to_cmyk(img_filename)

        im_width, im_height = get_picture_size(img_filename)

        if coords.source == "5_consommation-d-energie":
            if im_width < 5000:
                coords["crop_right"] = 70

        if coords["crop"] == "oui":
            print("\t - Cropping picture")
            img_filename = crop_picture(
                img_filename,
                coords["crop_left"],
                coords["crop_right"],
                coords["crop_top"],
                coords["crop_bottom"],
            )

        # http://pymupdf.readthedocs.io/en/latest/rect/
        # Set position and size according to your needs
        img_rect = fitz.Rect(
            coords["x"],
            coords["y"],
            coords["x"] + coords["width"],
            coords["y"] + coords["height"],
        )

        page.draw_rect(img_rect, color=getColor("white"), fill=getColor("white"))

        page.insert_image(img_rect, filename=img_filename)

    # See http://pymupdf.readthedocs.io/en/latest/document/#Document.save and
    # http://pymupdf.readthedocs.io/en/latest/document/#Document.saveIncr for
    # additional parameters, especially if you want to overwrite existing PDF
    # instead of writing new PDF
    document.save(dst_pdf_filename)

    document.close()

    compression_command = f"{GS_EXEC_NAME} -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile={compressed_dst_pdf_filename} {dst_pdf_filename}"
    os.system(compression_command)
