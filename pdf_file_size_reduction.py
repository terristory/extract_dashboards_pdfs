import os
import sys
import argparse

# Windows version (/!\ add to path /!\)
GS_EXEC_NAME = "gswin64c.exe"
# Linux version
GS_EXEC_NAME = "gs"

def convertir(fichier_a_convertir, repertoire, repertoire_destination):
    """
    Convertit un fichier à saisir en paramètre de la fonction
    """
    commande = GS_EXEC_NAME + " -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=./" + repertoire_destination + "/" + fichier_a_convertir + " " + repertoire + fichier_a_convertir
    print("Executes", commande)
    os.system(commande)

def est_un_pdf(fichier):
    # vérifie que l'extension est bien pdf
    return fichier.split('.')[-1] == 'pdf'

def execution(repertoire, repertoire_destination):
    """
    Exécute la commande sur une liste de répertoires
    """
    # pour chacun des fichiers du répertoire, on convertit
    for fichier in os.listdir(repertoire):
        if est_un_pdf(fichier):
            convertir(fichier, repertoire, repertoire_destination)

if __name__ == "__main__":
    # On récupère le dossier contenant les pdfs
    repertoire = "outputs/steps/2_pictures/"

    # On utilise le dossier cible sauf si envoyé en paramètres
    repertoire_destination = "outputs/steps/3_pdf/"
    
    # On vérifie que le dossier de pdfs existe
    if not os.path.isdir(repertoire):
        raise FileNotFoundError("Le dossier de pdfs n'existe pas.")
    
    # On vérifie aussi que le dossier cible existe, sinon on le crée
    if not os.path.isdir(repertoire_destination):
        print("Crée le répertoire de destination.")
        os.mkdir(repertoire_destination)
    
    BOLD = "\033[1m"
    END = '\033[0m'
    print("Vous allez importer les données de", BOLD + repertoire + END, "vers", BOLD + repertoire_destination + END)

    # On lance
    execution(repertoire, repertoire_destination)