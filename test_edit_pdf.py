"""
Just a small file to test editing a PDF file.
"""
from pypdf import PdfWriter, PdfReader

writer = PdfWriter()
reader = PdfReader("original_example.pdf")
watermark = PdfReader("image.pdf")

page = reader.pages[0]
page.merge_page(watermark.pages[0])
writer.add_page(page)

# finally, write the results to disk
with open("output.pdf", "wb") as fp:
    writer.write(fp)



"""
Other test with picture
https://stackoverflow.com/a/35218544
"""
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4

# there are 66 slides (1.jpg, 2.jpg, 3.jpg...)
path = 'image.jpg'
pdf = PdfWriter()

# Using ReportLab Canvas to insert image into PDF
imgTemp = BytesIO()
imgDoc = canvas.Canvas(imgTemp, pagesize=A4)
# Draw image on Canvas and save PDF in buffer
imgDoc.drawImage(path, -25, -45)
# x, y - start position
# in my case -25, -45 needed
imgDoc.save()
# Use PyPDF to merge the image-PDF into the template
pdf.add_page(PdfReader(BytesIO(imgTemp.getvalue())).pages[0])
pdf.write(open("output2.pdf","wb"))


"""
Third test
"""

import fitz

src_pdf_filename = 'original_example.pdf'
dst_pdf_filename = 'output3.pdf'
img_filename = 'image.jpg'

# http://pymupdf.readthedocs.io/en/latest/rect/
# Set position and size according to your needs
img_rect = fitz.Rect(200, 200, 500, 500)

document = fitz.open(src_pdf_filename)

# We'll put image on first page only but you could put it elsewhere
page = document[0]
page.insert_image(img_rect, filename=img_filename)

# See http://pymupdf.readthedocs.io/en/latest/document/#Document.save and
# http://pymupdf.readthedocs.io/en/latest/document/#Document.saveIncr for
# additional parameters, especially if you want to overwrite existing PDF
# instead of writing new PDF
document.save(dst_pdf_filename)

document.close()
