# Extract Dashboards PDFs

Tool to automatically extract all EPCI main dashboard PDF using `selenium`.

## TerriSTORY® version and database

* Use `fiche_territoriale` database (warning, other regions tables are sometimes truncated)
* Use `2023-territorial-file` at tag `territorial-file-generator` for TerriSTORY® version

## Requirements

This script uses at least Python 3.8.

All dependencies are listed inside `requirements.txt` file. You can run `pip install -r requirements.txt` inside your
own Python 3.8+ virtual env.

## Data Requirements
### How to Run the Database Creation Script

This repository contains a script, **`sh run_database.sh`**, which automates the creation of required databases and tables. Follow the steps below to use it effectively:

1. **Modify `config.json`**:
   - Update the database connection parameters (e.g., host, port, username, password) as needed.
   - Specify the directory path parameters inside `config.json` to match your system configuration.

2. **Navigate to the Database Directory**:
   - Ensure you are inside the `database` directory before executing the script.
   - Run the script as follows:

     ```bash
     sh run_database.sh
     ```

3. **What the Script Does**:
   - Creates the following tables automatically:
     - `indicateurs`
     - `categorie`
     - `chart`
     - `indicateur_representation_details`
     - `tableau_bord`
     - `tableau_de_bord_thematique`

   - Identifies and creates any missing tables required for the application using **`SCRIPT_CREATE_MISSING_TABLES.sql`**

4. **Handling Missing Tables**:
   - If additional data tables used in `meta.indicateurs` are missing, add them to **`SCRIPT_CREATE_MISSING_TABLES.sql`** to define and add all the required tables.
   - After updating the missing tables, run the `run_database.sh` script again to complete the database setup.

## Other Configurations
Ensure all parameters are correctly set and all data tables used in `meta.indicateur` exists in regional shchema to avoid errors during execution. With the updated configuration and the provided scripts, the database and related tables will be set up efficiently.
