import fitz
from PIL import Image, ImageDraw, ImageFont


src_pdf_filename = 'templates/original_example.pdf'
dst_pdf_filename = 'outputs/output4.pdf'

# http://pymupdf.readthedocs.io/en/latest/rect/
# Set position and size according to your needs
document = fitz.open(src_pdf_filename)

# We'll put image on first page only but you could put it elsewhere
page = document[0]

p = fitz.Point(50, 72)  # start point of 1st line
rc = page. insert_font(fontname='Montserrat', fontfile="fonts/Montserrat-Bold.ttf")

rc = page.insert_text(p,  # bottom-left of 1st char
                     "Ceci est le nom de l'EPCI !",  # the text (honors '\n')
                     fontname = "Montserrat",  # the default font
                     fontsize = 18,  # the default font size
                     rotate = 0,  # also available: 90, 180, 270
                     )
# See http://pymupdf.readthedocs.io/en/latest/document/#Document.save and
# http://pymupdf.readthedocs.io/en/latest/document/#Document.saveIncr for
# additional parameters, especially if you want to overwrite existing PDF
# instead of writing new PDF
document.save(dst_pdf_filename)

document.close()
