﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import os
import pandas as pd
import requests
from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import UnexpectedAlertPresentException
import slugify
import time
import shutil
import zipfile
import json


# Get the type of territory from config.json
with open("config.json", "r") as f:
    config = json.load(f)


DASHBOARD_ID = config["dashboard_id"]
TYPE = config["territory_type"]  # Change this to "departement" or "epci" as needed in config.json

# List of valid types and corresponding parameters for the URL
valid_types = ["epci", "departement", "scot"]
zone_mapping = {
    "epci": "epci",
    "departement": "departement",
    "scot": "scot"
}
maille_mapping = {
    "epci": "commune",
    "departement": "epci",
    "scot": "epci"
}
csv_mapping = {
    "epci": "epci.csv",
    "departement": "departement.csv",
    "scot": "scot.csv"
}

# Check if the TYPE is valid
if TYPE not in valid_types:
    raise ValueError("Invalid type of territory")

# Dynamically construct the URL and load the corresponding CSV file
DASHBOARD_URL = f"http://localhost:3000/restitution_tableaux_bord?zone={zone_mapping[TYPE]}&maille={maille_mapping[TYPE]}&zone_id={{code}}&id_tableau={{dashboard_id}}&nom_territoire={{nom}}"
territories_list = pd.read_csv(f"inputs/csv/{csv_mapping[TYPE]}", sep=";", dtype={'code': str})

opts = webdriver.FirefoxOptions()
opts.add_argument("--width=1600")
opts.add_argument("--height=1080")
opts.set_preference("browser.download.folderList", 2)
opts.set_preference(
    "browser.download.dir",
    f"{config['directory_path']}/screenshots/",
)
opts.set_preference("browser.download.useDownloadDir", True)
opts.set_preference("browser.download.viewableInternally.enabledTypes", "")
opts.set_preference(
    "browser.helperApps.neverAsk.saveToDisk",
    "application/pdf;text/plain;application/text;text/xml;application/xml",
)
opts.set_preference("pdfjs.disabled", True)

browser = webdriver.Firefox(
    service=Service(GeckoDriverManager().install()), options=opts
)


def wait_for_action(browser, type_waiting, class_name):
    # WebDriverWait attend que la balise soit présente pour passer à la suite du script.
    # Si après le temps imparti, la balise n'est toujours pas sur l'interface, une erreur est levée.
    if type_waiting == "present":
        WebDriverWait(browser, 15).until(
            EC.presence_of_element_located((By.CLASS_NAME, class_name))
        )
    elif type_waiting == "absent":
        WebDriverWait(browser, 15).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, class_name))
        )
    elif type_waiting.startswith("at_least"):
        nb_blocks = type_waiting[len("at_least(") : -1]

        def waiting_func(driver):
            list_blocks = driver.find_elements(By.CLASS_NAME, class_name)
            return len(list_blocks) >= int(nb_blocks)

        WebDriverWait(browser, 15).until(lambda driver: waiting_func(driver))


for index, epci in territories_list.iterrows():
    code = str(epci.code)
    url = DASHBOARD_URL.format(dashboard_id=DASHBOARD_ID, **epci)
    print("\t - Goes to", url)
    nom = slugify.slugify(epci.nom)

    # we can try more than one time
    tries = 0
    max_tries = 3
    succeeded = False
    while tries < max_tries and not succeeded:
        try:
            # Loads page
            browser.get(url)
            wait_for_action(browser, "at_least(20)", "block-row")
            succeeded = True

            # on attend la fin des animations

            time.sleep(4)
            # print("\t\t - Zooms in")
            # browser.execute_script("document.body.style.zoom='170%'")
            # browser.execute_script('document.body.style.MozTransform = "scale(1.7)";')
            # browser.execute_script('document.body.style.MozTransformOrigin = "0 0";')
            time.sleep(10)

            print("Is going to look for a button")
            # recherche un element par le nom de sa classe
            download_button = browser.find_element(
                by=By.ID, value="download-all-graphs"
            )
            print("download_button", download_button)
            # clique sur l'élément
            download_button.click()

            # on attend la fin des animations
            time.sleep(5)
            if not os.path.isfile("screenshots/total.zip"):
                time.sleep(10)

            # on attend la fin des animations
            time.sleep(2)
            # shutil.copyfile(
            #     "screenshots/total.zip",
            #     f"/home/mdenoux/NextcloudAURA-EE/Operations/230_Convention_DEE_ADEME_2023/23014_TerriSTORY_en_AURA/LIVRABLES/dashboard_{code}_{nom}.zip"
            # )
            os.rename(
                "screenshots/total.zip",
                f"screenshots/dashboard_{code}_{nom}.zip",
            )

            with zipfile.ZipFile(
                f"screenshots/dashboard_{code}_{nom}.zip", "r"
            ) as zip_ref:
                zip_ref.extractall(f"inputs/graphs/{code}/")

        # another try is authorized when exception is raised
        except UnexpectedAlertPresentException as e:
            print("\t\t" + ("##" * 40))
            print("\t\t" + ("\n\t\t".join(str(e).split("\n"))))
            print("\t\t" + ("##" * 40))
            print(f"\t\t Tries reloading ({tries+1} try)")
            print("\t\t" + ("##" * 40))
            time.sleep(1)
            browser.refresh()
        tries += 1

browser.close()
